#!/bin/bash
 rm /tmp/logimage
 pre_url="$(curl -L 'https://c.xkcd.com/random/comic/' |grep -Eo 'imgs.xkcd.com/comics/[a-zA-Z_]*.[a-zA-Z]*' |head -n 1)"
 url="https://$pre_url"
 logger "Locking screen with $url"
 wget "$url" -O /tmp/lockimage -o /dev/null || exit 1
 ls /tmp/lockimage || exit 2
 i3lock -i /tmp/lockimage || exit 3
